from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

def kreirajSesiju():
    engine = create_engine("sqlite:///Assets/BazaPodataka.db")
    DBSession = sessionmaker(bind=engine)
    sesija = DBSession()
    return sesija