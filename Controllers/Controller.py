from Assets.Sesiomat import kreirajSesiju
from Views.GlavniProzor import GlavniProzor
from Views.PopisPacijenata import PopisPacijenata
from Views.UnosPacijenta import UnosPacijenta
from Views.PopisLijecnika import PopisLijecnika
from Views.UnosLijecnika import UnosLijecnika
from Views.IzmjenaPacijenta import IzmjenaPacijenta
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class Controller:
    def __init__(self):
        self.sesija = kreirajSesiju()

        self.glavniProzorPogled = GlavniProzor(self.popisPacijenataFunkcija, 
                                               self.unosPacijentaFunkcija,  self.popisLijecnikaFunkcija, 
                                               self.unosLijecnikaFunkcija)
        self.popisPacijenataPogled = PopisPacijenata(self.glavniProzorFunkcija, self.brisanjePacijentaFunkcija, self.popisPacijenataFunkcija,
                                                     self.spremanjeIzmjenaPacijentaFunkcija, self.sviLijecnici, self.povijestBolestiDijagnoza)
        self.unosPacijenataPogled = UnosPacijenta(self.glavniProzorFunkcija, self.spremanjePacijentaFunkcija, self.oibPostojiPacijentFunkcija)
        self.popisLijecnikaPogled = PopisLijecnika(self.glavniProzorFunkcija, self.brisanjeLijecnikaFunkcija, self.popisLijecnikaFunkcija,
                                                   self.popisPacijenataFunkcija, self.spremanjeIzmjenaPacijentaFunkcija, self.sviLijecnici,
                                                     self.povijestBolestiDijagnoza, self.spremanjeIzmjenaLijecnikaFunkcija, self.brisanjePacijentaFunkcija,
                                                     self.pacijentiOdabranogLijecnikaFunkcija, self.odabraniLijecnikFunkcija)
        self.unosLijecnikaPogled = UnosLijecnika(self.glavniProzorFunkcija, self.spremanjeLijecnikaFunkcija, self.oibPostojiLijecnikFunkcija)
        
        self.glavniProzorFunkcija()

    def glavniProzorFunkcija(self):
        self.glavniProzorPogled.show()
        self.popisPacijenataPogled.hide()
        self.unosPacijenataPogled.hide()
        self.popisLijecnikaPogled.hide()
        self.unosLijecnikaPogled.hide()
    
    def popisPacijenataFunkcija(self):

        popispacijenata = self.sesija.query(Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB,
                                    Pacijent.ADRESA, Pacijent.DATUMRODENJA, Pacijent.SPOL,
                                    Pacijent.BROJOSIGURANEOSOBE, Pacijent.TELEFON,
                                    Pacijent.EMAIL, Lijecnik.IME,
                                    Lijecnik.PREZIME).join(Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID).all()
        self.popisPacijenataPogled.postaviPacijente(popispacijenata)

        self.glavniProzorPogled.hide()
        self.popisPacijenataPogled.show()
        self.unosPacijenataPogled.hide()
        self.popisLijecnikaPogled.hide()
        self.unosLijecnikaPogled.hide()

    def unosPacijentaFunkcija(self):
        lijecnici = self.sesija.query(Lijecnik).all()
        self.unosPacijenataPogled.postaviCombo(lijecnici)

        self.glavniProzorPogled.hide()
        self.popisPacijenataPogled.hide()
        self.unosPacijenataPogled.show()
        self.popisLijecnikaPogled.hide()
        self.unosLijecnikaPogled.hide()

    def popisLijecnikaFunkcija(self):

        popislijecnika = self.sesija.query(Lijecnik.IME, Lijecnik.PREZIME, Lijecnik.OIB,
                                Lijecnik.ADRESA, Lijecnik.DATUMRODENJA, Lijecnik.SPOL,
                                Lijecnik.SPECIJALIZACIJA, Lijecnik.REGBROJ, Lijecnik.TELEFON,
                                Lijecnik.EMAIL).all()
        self.popisLijecnikaPogled.postaviLijecnike(popislijecnika)

        self.glavniProzorPogled.hide()
        self.popisPacijenataPogled.hide()
        self.unosPacijenataPogled.hide()
        self.popisLijecnikaPogled.show()
        self.unosLijecnikaPogled.hide()

    def unosLijecnikaFunkcija(self):
        self.glavniProzorPogled.hide()
        self.popisPacijenataPogled.hide()
        self.unosPacijenataPogled.hide()
        self.popisLijecnikaPogled.hide()
        self.unosLijecnikaPogled.show()
    
    def brisanjePacijentaFunkcija(self,oib):       
        pacijentZaBrisanje = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()
        self.sesija.delete(pacijentZaBrisanje)
        self.sesija.commit()
        return
        
    def brisanjeLijecnikaFunkcija(self,oib):
        lijecnikKojiSeBrise = self.sesija.query(Lijecnik).filter_by(OIB=oib).first()

        if lijecnikKojiSeBrise:
        
            self.sesija.delete(lijecnikKojiSeBrise)
            self.sesija.query(Pacijent).filter_by(ODABIRDOKTORA=lijecnikKojiSeBrise.ID).update({Pacijent.ODABIRDOKTORA: 0})
            self.sesija.commit()  
        return
    
    def spremanjePacijentaFunkcija(self,noviPacijent):
         self.sesija.add(noviPacijent)
         self.sesija.commit()

    def spremanjeLijecnikaFunkcija(self,noviLijecnik):
        self.sesija.add(noviLijecnik)
        self.sesija.commit()

    def spremanjeIzmjenaPacijentaFunkcija(self, ime, prezime, oib, adresa, datumRodjenja, spol,
                                           brOsigurane, telefon, email, odabraniLijecnikId, povijestBolesti, dijagnoza):
        pacijent = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()

        pacijent.IME = ime
        pacijent.PREZIME = prezime
        pacijent.OIB = oib
        pacijent.ADRESA = adresa
        pacijent.DATUMRODENJA = datumRodjenja
        pacijent.SPOL = spol
        pacijent.BROJOSIGURANEOSOBE = brOsigurane
        pacijent.TELEFON = telefon
        pacijent.EMAIL = email
        pacijent.ODABIRDOKTORA = odabraniLijecnikId
        pacijent.POVIJESTBOLESTI = povijestBolesti
        pacijent.DIJAGNOZA = dijagnoza

        self.sesija.commit()
    
    def spremanjeIzmjenaLijecnikaFunkcija(self, ime, prezime, oib, adresa, datumRodjenja,
                                           spol, specijalizacija, regBroj, telefon, email):
        lijecnik = self.sesija.query(Lijecnik).filter(Lijecnik.OIB == oib).first()

        lijecnik.IME = ime
        lijecnik.PREZIME = prezime
        lijecnik.OIB = oib
        lijecnik.ADRESA = adresa
        lijecnik.DATUMRODENJA = datumRodjenja
        lijecnik.SPOL = spol
        lijecnik.SPECIJALIZACIJA = specijalizacija
        lijecnik.REGBROJ = regBroj
        lijecnik.TELEFON = telefon
        lijecnik.EMAIL = email
                        
        self.sesija.commit()
    
    def sviLijecnici(self):
        lijecnici = self.sesija.query(Lijecnik).all()
        return lijecnici
    
    def povijestBolestiDijagnoza(self,oib):
        povijestBolesti = self.sesija.query(Pacijent.POVIJESTBOLESTI).filter(Pacijent.OIB == oib).all()
        dijagnoza = self.sesija.query(Pacijent.DIJAGNOZA).filter(Pacijent.OIB == oib).all()
        return(povijestBolesti,dijagnoza)

    def oibPostojiLijecnikFunkcija(self,oib,regBroj):
        oibPostoji = self.sesija.query(Lijecnik).filter(Lijecnik.OIB == oib).first()
        regBrojPostoji = self.sesija.query(Lijecnik).filter(Lijecnik.REGBROJ == regBroj).first()
        return(oibPostoji,regBrojPostoji)
    
    def oibPostojiPacijentFunkcija(self,oib,brOsigurane):
        oibPostoji = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()
        brOsiguranePostoji = self.sesija.query(Pacijent).filter(Pacijent.BROJOSIGURANEOSOBE == brOsigurane).first()
        return(oibPostoji,brOsiguranePostoji)

    def pacijentiOdabranogLijecnikaFunkcija(self, idOdabranogLijecnika):
        redovi = self.sesija.query(
            Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB, Pacijent.ADRESA,
            Pacijent.DATUMRODENJA, Pacijent.SPOL, Pacijent.BROJOSIGURANEOSOBE,
            Pacijent.TELEFON, Pacijent.EMAIL, Lijecnik.IME, Lijecnik.PREZIME
        ).join(
            Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID
        ).filter(
            Pacijent.ODABIRDOKTORA == idOdabranogLijecnika
        ).all()
        return redovi

    def odabraniLijecnikFunkcija(self,oib):
        odabraniLijecnik = self.sesija.query(Lijecnik).filter_by(OIB=oib).first()
        return odabraniLijecnik


   