from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Views.IzmjenaPacijenta import IzmjenaPacijenta
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent


class PopisPacijenata(QWidget):
    def __init__(self,povratakGlavniProzor, brisanjePacijentaFunkcija,
                  popisPacijenataFunkcija, spremanjeIzmjenaPacijentaFunkcija, sviLijecnici, povijestBolestiDijagnoza):
        super().__init__()
        uic.loadUi("./Views/popisPacijenata.ui",self)
       
        self.brisanje = brisanjePacijentaFunkcija
        self.popis = popisPacijenataFunkcija
        self.spremanjeIzmjenaPacijentaFunkcija = spremanjeIzmjenaPacijentaFunkcija
        self.sviLijecnici = sviLijecnici
        self.povijestBolestiDijagnoza = povijestBolestiDijagnoza
               
        self.izlazButton.clicked.connect(povratakGlavniProzor)
        self.brisanjeButton.clicked.connect(self.brisanjePacijenta)  
        self.izmjenaButton.clicked.connect(self.izmjenaPacijenta)  

    def brisanjePacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati pacijenta {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        
        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        response = odgovor.exec_()

        if response == QMessageBox.No:
            return
        
        self.brisanje(oib)
        
        
        self.tablica.removeRow(odabraniRed)       


    def izmjenaPacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
                
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        brOsigurane = self.tablica.item(odabraniRed, 6).text()
        telefon = self.tablica.item(odabraniRed, 7).text()
        email = self.tablica.item(odabraniRed, 8).text()
        odabraniLijecnik = self.tablica.item(odabraniRed, 9).text()
        
        self.prozorIzmjenaPacijenta = IzmjenaPacijenta(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, brOsigurane, telefon, email,
                                                        odabraniLijecnik, self.popis,
                                                          self.spremanjeIzmjenaPacijentaFunkcija, self.sviLijecnici, self.povijestBolestiDijagnoza)
        
                  
        
        self.prozorIzmjenaPacijenta.show()
        self.hide()
    

    def postaviPacijente(self, popispacijenata):
        self.tablica.setRowCount(len(popispacijenata))
        trenutniRedak = 0
        for redak in popispacijenata:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            trenutniRedak += 1
       
    
        self.tablica.setCurrentCell(0, -1)
      
