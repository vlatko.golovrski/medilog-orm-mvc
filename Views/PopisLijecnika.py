from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Views.IzmjenaLijecnika import IzmjenaLijecnika
from Views.PacijentiOdabranog import PacijentiOdabranog

class PopisLijecnika(QWidget):
    def __init__(self,povratakGlavniProzor, brisanjeLijecnikaFunkcija, 
                 popisLijecnikaFunkcija, popisPacijenataFunkcija, spremanjeIzmjenaPacijentaFunkcija,
                   sviLijecnici, povijestBolestiDijagnoza, spremanjeIzmjenaLijecnikaFunkcija, brisanjePacijentaFunkcija,
                   pacijentiOdabranogLijecnikaFunkcija, odabraniLijecnikFunkcija):
        super().__init__()
        uic.loadUi("./Views/popisLijecnika.ui",self)
        
        self.brisanje = brisanjeLijecnikaFunkcija
        self.popis = popisLijecnikaFunkcija
        self.popisPacijenataFunkcija = popisPacijenataFunkcija
        self.spremanjeIzmjenaPacijentaFunkcija = spremanjeIzmjenaPacijentaFunkcija
        self.sviLijecnici = sviLijecnici
        self.povijestBolestiDijagnoza = povijestBolestiDijagnoza
        self.spremanjeIzmjenaLijecnikaFunkcija = spremanjeIzmjenaLijecnikaFunkcija
        self.brisanjePacijentaFunkcija = brisanjePacijentaFunkcija
        self.pacijentiOdabranogLijecnikaFunkcija = pacijentiOdabranogLijecnikaFunkcija
        self.odabraniLijecnikFunkcija = odabraniLijecnikFunkcija


        self.izlazButton.clicked.connect(povratakGlavniProzor)
        self.brisanjeButton.clicked.connect(self.brisanjeLijecnika)
        self.izmjenaButton.clicked.connect(self.izmjenaLijecnika)
        self.pacijentiOdabranogButton.clicked.connect(self.pacijentiOdabranog)
        
        
    def brisanjeLijecnika(self):
        
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati liječnika {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")
        response = odgovor.exec_()

        if response == QMessageBox.No:
            return
                
        self.brisanje(oib)

      
        self.tablica.removeRow(odabraniRed)

    def izmjenaLijecnika(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        specijalizacija = self.tablica.item(odabraniRed, 6).text()
        regBroj = self.tablica.item(odabraniRed, 7).text()
        telefon = self.tablica.item(odabraniRed, 8).text()
        email = self.tablica.item(odabraniRed, 9).text()
 
        self.prozorOdabraniLijecnik = IzmjenaLijecnika(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, specijalizacija, regBroj, telefon,
                                                          email,self.popis, 
                                                          self.spremanjeIzmjenaLijecnikaFunkcija)
        self.prozorOdabraniLijecnik.show()
        self.hide()

    def pacijentiOdabranog(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
 
        oib = self.tablica.item(odabraniRed, 2).text()

        odabraniLijecnik = self.odabraniLijecnikFunkcija(oib)
        if not odabraniLijecnik:
            return

        idOdabranogLijecnika = odabraniLijecnik.ID

        self.prozorOdabraniLijecnik = PacijentiOdabranog(idOdabranogLijecnika, self.popis,
                                                         self.popisPacijenataFunkcija, self.spremanjeIzmjenaPacijentaFunkcija,
                                                           self.sviLijecnici, self.povijestBolestiDijagnoza, self.brisanjePacijentaFunkcija,
                                                           odabraniLijecnik,
                                                            self.pacijentiOdabranogLijecnikaFunkcija)
        self.prozorOdabraniLijecnik.show()
        self.hide()
        
    def postaviLijecnike(self, popislijecnika):
        self.tablica.setRowCount(len(popislijecnika)-1)
        trenutniRedak = -1
        for redak in popislijecnika:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]))
            trenutniRedak += 1
        
        self.tablica.setCurrentCell(0, -1)
        