from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Pacijent import Pacijent
from Models.Lijecnik import Lijecnik

class UnosPacijenta(QWidget):
    def __init__(self, povratakGlavniProzor, spremanjePacijentaFunkcija, oibPostojiPacijentFunkcija) :
        super().__init__()
        uic.loadUi("./Views/unosPacijenta.ui",self)
        
        self.spremanje = spremanjePacijentaFunkcija
        self.oibPostojiPacijentFunkcija = oibPostojiPacijentFunkcija

        self.izlazButton.clicked.connect(povratakGlavniProzor)
        self.brisiButton.clicked.connect(self.obrisi)
        self.SpremiButton.clicked.connect(self.unosPacijenta)
            
    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)
        porukaProzor.exec()    
    

    def unosPacijenta(self):
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.datum_text.text().strip()
        spol = self.spol_combo.currentText()
        brOsigurane = self.brojOsigurane_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.email_text.text().strip()
        povijestBolesti = self.PovijestBolesti_text.toPlainText().strip()     
        odabraniLijecnikId = self.odabirDoktora_combo.currentData(Qt.UserRole)
        
        try:        
            if len(oib) != 11 or not oib.isdigit():
                raise ValueError("OIB mora imati jedanaest znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        try:        
            if len(brOsigurane) != 8 or not brOsigurane.isdigit():
                raise ValueError("Broj osigurane osobe mora imati 8 znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        
        oibPostoji,brOsiguranePostoji = self.oibPostojiPacijentFunkcija(oib,brOsigurane)
               
        if brOsiguranePostoji:
            self.porukaProzor("Postoji već pacijent s tim brojem osiguranja!")         
        elif oibPostoji:
            self.porukaProzor("Postoji već pacijent s tim OIB-om!")
        elif odabraniLijecnikId == None:
            self.porukaProzor("Morate dodijeliti liječnika pacijentu!")    
           
        else:
            try:
                noviPacijent = Pacijent(
                    IME = ime,PREZIME = prezime,
                    OIB = oib,ADRESA = adresa,
                    DATUMRODENJA = datumRodjenja,
                    SPOL = spol,
                    BROJOSIGURANEOSOBE = brOsigurane,
                    TELEFON = telefon,
                    EMAIL = email,
                    ODABIRDOKTORA = odabraniLijecnikId,
                    POVIJESTBOLESTI = povijestBolesti)
                self.spremanje(noviPacijent)
                
                self.porukaProzor(f"Pacijent {noviPacijent.IME} {noviPacijent.PREZIME} uspješno unesen!!","information")
                self.obrisi()
            
            except Exception as exc:
                self.porukaProzor(str(exc))      
          

    def obrisi(self):
        self.ime_text.setText("")
        self.prezime_text.setText("")
        self.oib_text.setText("")
        self.adresa_text.setText("")
        self.datum_text.setText("")
        self.spol_combo.setCurrentText("")
        self.brojOsigurane_text.setText("")
        self.telefon_text.setText("")
        self.email_text.setText("")
        self.odabirDoktora_combo.setCurrentText("")
        self.PovijestBolesti_text.setText("")

    def postaviCombo(self, lijecnici):
        self.odabirDoktora_combo.clear()        
        self.odabirDoktora_combo.addItem("")
        for lijecnik in lijecnici:
            imePrezimeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            doctor_id = lijecnik.ID
            self.odabirDoktora_combo.addItem(imePrezimeLijecnika, userData=doctor_id)  