from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Views.IzmjenaPacijenta import IzmjenaPacijenta

class PacijentiOdabranog(QWidget):
    def __init__(self, IdOdabranogLijecnika,povratak, popisPacijenataFunkcija,
                  spremanjeIzmjenaPacijentaFunkcija, sviLijecnici, povijestBolestiDijagnoza, 
                  brisanjePacijentaFunkcija, odabraniLijecnik, pacijentiOdabranogLijecnikaFunkcija):
        super().__init__()
        uic.loadUi("./Views/pacijentiOdabranog.ui",self)
        
        self.povratak = povratak
        self.popis = popisPacijenataFunkcija
        self.spremanjeIzmjenaPacijentaFunkcija = spremanjeIzmjenaPacijentaFunkcija
        self.sviLijecnici = sviLijecnici
        self.povijestBolestiDijagnoza = povijestBolestiDijagnoza
        self.brisanjePacijentaFunkcija = brisanjePacijentaFunkcija
        self.pacijentiOdabranogLijecnikaFunkcija = pacijentiOdabranogLijecnikaFunkcija

        self.IdOdabranogLijecnika = IdOdabranogLijecnika
        self.izlazButton.clicked.connect(povratak)
        self.izlazButton.clicked.connect(self.close)
        self.brisanjeButton.clicked.connect(self.brisanjePacijenta)
        self.izmjenaButton.clicked.connect(self.izmjenaPacijenta) 
        
        
        self.ime = odabraniLijecnik.IME
        self.prezime = odabraniLijecnik.PREZIME        
        self.pacijentiLijecnikaLabel.setText(f"Pacijenti liječnika {self.ime} {self.prezime}")
        self.pacijentiLijecnikaLabel.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        redovi = self.pacijentiOdabranogLijecnikaFunkcija(IdOdabranogLijecnika)

    
        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            
            trenutniRedak += 1

        self.tablica.setCurrentCell(0, -1)        

    def brisanjePacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati pacijenta {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        
        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        response = odgovor.exec_()

        if response == QMessageBox.No:
            return
        
        self.brisanjePacijentaFunkcija(oib)

        self.tablica.removeRow(odabraniRed)

    def izmjenaPacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        brOsigurane = self.tablica.item(odabraniRed, 6).text()
        telefon = self.tablica.item(odabraniRed, 7).text()
        email = self.tablica.item(odabraniRed, 8).text()
        odabraniLijecnik = self.tablica.item(odabraniRed, 9).text()

        self.prozorIzmjenaPacijenta = IzmjenaPacijenta(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, brOsigurane, telefon, email,
                                                        odabraniLijecnik, self.show, self.spremanjeIzmjenaPacijentaFunkcija,
                                                          self.sviLijecnici, self.povijestBolestiDijagnoza)
        self.prozorIzmjenaPacijenta.show()
        self.hide()
        self.prozorIzmjenaPacijenta.data_changed.connect(self.refresh_table)
        
    
    def refresh_table(self):

        
        redovi = self.pacijentiOdabranogLijecnikaFunkcija(self.IdOdabranogLijecnika)

        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            
            trenutniRedak += 1