from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic


class IzmjenaPacijenta(QWidget):
    data_changed = pyqtSignal()
    
    def __init__(self,ime, prezime, oib, adresa, datumRodenja,
                  spol, brOsigurane, telefon, email, odabraniLijecnik,
                    povratak, spremanjeIzmjenaPacijentaFunkcija, sviLijecnici,
                    povijestBolestiDijagnoza):
        super().__init__()
        uic.loadUi("./Views/izmjenaPacijenta.ui",self)
        
        self.povratak= povratak
        self.spremanjeIzmjenaPacijentaFunkcija = spremanjeIzmjenaPacijentaFunkcija
        self.sviLijecnici = sviLijecnici
        self.povijestBolestiDijagnoza = povijestBolestiDijagnoza
        

        self.ime_text.setText(ime)
        self.prezime_text.setText(prezime)
        self.oib_text.setText(oib)
        self.adresa_text.setText(adresa)
        self.datum_text.setText(datumRodenja)
        self.spol_combo.setCurrentText(spol)
        self.brojOsigurane_text.setText(brOsigurane)
        self.telefon_text.setText(telefon)
        self.email_text.setText(email)

        self.odabirDoktora_combo.clear()        
        self.odabirDoktora_combo.addItem("")

        lijecnici = self.sviLijecnici()
        for lijecnik in lijecnici:
            imePrezimeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            doctor_id = lijecnik.ID
            self.odabirDoktora_combo.addItem(imePrezimeLijecnika, userData=doctor_id)        
        
        self.odabirDoktora_combo.setCurrentText(odabraniLijecnik)       
        
        (povijestBolesti,dijagnoza) = self.povijestBolestiDijagnoza(oib)
        
        self.PovijestBolesti_text.setText('\n'.join(str(item[0]) for item in povijestBolesti))
        self.Dijagnoza_text.setText('\n'.join(str(item[0]) for item in dijagnoza))

        self.izlazButton.clicked.connect(povratak)
        self.izlazButton.clicked.connect(self.close)
        
        self.SpremiButton.clicked.connect(self.spremiIzmjene)
        self.SpremiButton.clicked.connect(povratak)

    
    def spremiIzmjene(self):
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.datum_text.text().strip()
        spol = self.spol_combo.currentText()
        brOsigurane = self.brojOsigurane_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.email_text.text().strip()
        odabraniLijecnikId = self.odabirDoktora_combo.currentData(Qt.UserRole)
        povijestBolesti = self.PovijestBolesti_text.toPlainText()
        dijagnoza = self.Dijagnoza_text.toPlainText()

        if odabraniLijecnikId == None:
            self.porukaProzor("Morate dodijeliti liječnika pacijentu!")
        else:
            try:

                self.spremanjeIzmjenaPacijentaFunkcija(ime, prezime, oib, adresa, datumRodjenja, spol,
                                                  brOsigurane, telefon, email, odabraniLijecnikId,
                                                  povijestBolesti, dijagnoza)
                                                    
                self.porukaProzor(f"Pacijent {ime} {prezime} uspješno izmjenjen!!",
                                  "information")
                self.data_changed.emit()
                self.close()

            except Exception as exc:
                self.porukaProzor(str(exc))

    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)

        porukaProzor.exec()

    