from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Lijecnik import Lijecnik


class UnosLijecnika(QWidget):
    def __init__(self,povratakGlavniProzor, spremanjeLijecnikaFunkcija, oibPostojiLijecnikFunkcija) :
        super().__init__()
        uic.loadUi("./Views/unosLijecnika.ui",self)
        
        self.spremanje = spremanjeLijecnikaFunkcija
        self.oibPostojiLijecnikFunkcija = oibPostojiLijecnikFunkcija

        self.izlazButton.clicked.connect(povratakGlavniProzor)
        self.SpremiButton.clicked.connect(self.unosLijecnika)
        self.brisiButton.clicked.connect(self.obrisi)

    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)

        porukaProzor.exec()

    def unosLijecnika(self):
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.dat_rod_text.text().strip()
        spol = self.spol_combo.currentText()
        specijalizacija = self.specijalizacija_text.text().strip()
        regBroj = self.regbroj_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.e_mail_text.text().strip()

        try:        
            if len(oib) != 11 or not oib.isdigit():
                raise ValueError("OIB mora imati jedanaest znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        try:        
            if len(regBroj) != 8 or not regBroj.isdigit():
                raise ValueError("Registracijski broj zdravstvenog djelatnika mora imati 8 znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        

        oibPostoji,regBrojPostoji = self.oibPostojiLijecnikFunkcija(oib,regBroj)
        
       
        if regBrojPostoji:
            self.porukaProzor("Postoji već liječnik s tim registarskim brojem!")         
        elif oibPostoji:
            self.porukaProzor("Postoji već liječnik s tim OIB-om!")

        else:
            try:
                noviLijecnik = Lijecnik(
                    IME = ime,PREZIME = prezime,
                    OIB = oib,ADRESA = adresa,
                    DATUMRODENJA = datumRodjenja,
                    SPOL = spol,
                    SPECIJALIZACIJA = specijalizacija,
                    REGBROJ = regBroj,
                    TELEFON = telefon,
                    EMAIL = email)
                
                self.spremanje(noviLijecnik)
                
                
                self.porukaProzor(f"Liječnik {noviLijecnik.IME} {noviLijecnik.PREZIME} uspješno unesen!!","information")
                self.obrisi()
        
            except Exception as exc:
                self.porukaProzor(str(exc))    
    
    def obrisi(self):
        self.ime_text.setText("")
        self.prezime_text.setText("")
        self.oib_text.setText("")
        self.adresa_text.setText("")
        self.dat_rod_text.setText("")
        self.spol_combo.setCurrentText("")
        self.specijalizacija_text.setText("")
        self.regbroj_text.setText("")
        self.telefon_text.setText("")
        self.e_mail_text.setText("")
        
     