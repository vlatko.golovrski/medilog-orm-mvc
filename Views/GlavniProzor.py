from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

class GlavniProzor(QMainWindow):
    def __init__(self, popisPacijenataFunkcija, unosPacijentaFunkcija, popisLijecnikaFunkcija, unosLijecnikaFunkcija):
        super().__init__()      

        uic.loadUi("./Views/glavniProzor.ui",self)

        self.B_Izlaz.clicked.connect(self.close)
        self.B_UnosPacijenta.clicked.connect(unosPacijentaFunkcija)
        self.B_PopisPacijenata.clicked.connect(popisPacijenataFunkcija)
        self.B_UnosLijecnika.clicked.connect(unosLijecnikaFunkcija)
        self.B_PopisLijecnika.clicked.connect(popisLijecnikaFunkcija)
     
   
