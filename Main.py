from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from Controllers.Controller import Controller

if __name__ == "__main__":
    app = QApplication([])
    controller = Controller()
    app.exec()